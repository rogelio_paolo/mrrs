# Generated by Django 2.2.2 on 2019-08-02 07:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0002_auto_20190802_1040'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('xero_id', models.CharField(blank=True, default=None, max_length=100, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='client',
            name='contents',
        ),
        migrations.RemoveField(
            model_name='client',
            name='pm',
        ),
        migrations.RemoveField(
            model_name='system',
            name='system_code',
        ),
        migrations.AddField(
            model_name='contentcreated',
            name='content_status',
            field=models.ForeignKey(blank=True, db_column='content_status_id', default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.System'),
        ),
        migrations.AddField(
            model_name='contentcreated',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='contentcreated',
            name='duration',
            field=models.ForeignKey(db_column='duration_id', default=1, on_delete=django.db.models.deletion.CASCADE, to='app.Duration'),
        ),
        migrations.AddField(
            model_name='contentcreated',
            name='pm',
            field=models.ForeignKey(blank=True, db_column='pm_id', default=None, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='pm', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contentcreated',
            name='service_created',
            field=models.ForeignKey(db_column='service_created_id', null=True, on_delete=django.db.models.deletion.CASCADE, to='app.ServiceCreated'),
        ),
        migrations.AddField(
            model_name='contentcreated',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='servicecreated',
            name='country',
            field=django_countries.fields.CountryField(default=None, max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='servicecreated',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='servicecreated',
            name='duration',
            field=models.ForeignKey(db_column='duration_id', default=1, on_delete=django.db.models.deletion.CASCADE, to='app.Duration'),
        ),
        migrations.AddField(
            model_name='servicecreated',
            name='pm',
            field=models.ForeignKey(blank=True, db_column='pm_id', default=None, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='servicecreated',
            name='service_status',
            field=models.ForeignKey(blank=True, db_column='service_status_id', default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.System'),
        ),
        migrations.AddField(
            model_name='servicecreated',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='invoice',
            field=models.ForeignKey(blank=True, db_column='invoice_id', default=None, null=True, on_delete=django.db.models.deletion.PROTECT, to='app.Invoice'),
        ),
    ]
