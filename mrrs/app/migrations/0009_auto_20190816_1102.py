# Generated by Django 2.2.2 on 2019-08-16 03:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0008_auto_20190808_1911'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='salesman',
        ),
        migrations.RemoveField(
            model_name='contentcreated',
            name='duration',
        ),
        migrations.RemoveField(
            model_name='contentcreated',
            name='pm',
        ),
        migrations.RemoveField(
            model_name='servicecreated',
            name='country',
        ),
        migrations.RemoveField(
            model_name='servicecreated',
            name='duration',
        ),
        migrations.AddField(
            model_name='client',
            name='pm',
            field=models.ForeignKey(blank=True, db_column='pm_id', default=None, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='client_pm', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='client',
            name='writer',
            field=models.ForeignKey(blank=True, db_column='writer_id', default=None, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='client_writer', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contentcreated',
            name='writer',
            field=models.ForeignKey(blank=True, db_column='writer_id', default=None, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='writer', to=settings.AUTH_USER_MODEL),
        ),
    ]
