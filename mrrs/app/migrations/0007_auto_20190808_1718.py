# Generated by Django 2.2.2 on 2019-08-08 09:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20190808_1531'),
    ]

    operations = [
        migrations.CreateModel(
            name='KpiCreated',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kpi', models.ForeignKey(db_column='kpi_id', on_delete=django.db.models.deletion.CASCADE, to='app.Kpi')),
            ],
        ),
        migrations.AlterField(
            model_name='client',
            name='kpi',
            field=models.ManyToManyField(to='app.KpiCreated'),
        ),
    ]
